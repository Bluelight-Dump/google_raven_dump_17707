## raven-user 12 SP2A.220405.004 8233519 release-keys
- Manufacturer: google
- Platform: gs101
- Codename: raven
- Brand: google
- Flavor: raven-user
- Release Version: 12
- Id: SP2A.220405.004
- Incremental: 8233519
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: true
- Locale: en-US
- Screen Density: undefined
- Fingerprint: google/raven/raven:12/SP2A.220405.004/8233519:user/release-keys
- OTA version: 
- Branch: raven-user-12-SP2A.220405.004-8233519-release-keys
- Repo: google_raven_dump_17707


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
